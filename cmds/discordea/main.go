// 		 __  __       _
// 		|  \/  |     (_)
// 		| \  / | __ _ _ _ __
// 		| |\/| |/ _` | | '_ \
// 		| |  | | (_| | | | | |
// 		|_|  |_|\__,_|_|_| |_|
//

package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/filippo.ziche/discordea/internal"
	"gitlab.com/filippo.ziche/discordea/internal/comands"
	"gitlab.com/filippo.ziche/discordea/internal/data"
)

// Quando il bot è pronto a ricevere comando
func ready(s *discordgo.Session, event *discordgo.Ready) {
	fmt.Println("Bot ready")
}

// Quando riceviamo un messaggio
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignora comandi dello stesso bot
	if m.Author.ID == s.State.User.ID {
		return
	}

	state := internal.STATE
	for _, comand := range state.Cmds {
		if comand.Activate(m.Content) {
			comand.Action(s, m)
		}
	}
}

func RegisterCallbacks(dg *discordgo.Session) {
	dg.AddHandler(messageCreate)
	dg.AddHandler(ready)
}

// Entry point
func main() {

	// NOTE Inizializza stato bot e inserisce comandi
	state := internal.InitializeState()
	state.Cmds = append(state.Cmds,
		data.CmdBuild([]string{"!ping"},
			comands.Ping))
	state.Cmds = append(state.Cmds,
		data.CmdBuild([]string{"!info", "!messaggio"},
			comands.Info))
	state.Cmds = append(state.Cmds,
		data.CmdBuild([]string{"!anima", "!soul"},
			comands.Anima))

	// NOTE Crea istanza del bot
	fmt.Printf("Creazione istanza bot ... ")
	token := "NzU2MDcxNzQyOTU4NjAwMjkz.X2Mgrg.dz5MKmgoqD6SQdWtqDaA0dNSIZw"
	bot, err := discordgo.New("Bot " + token)
	if err != nil {
		fmt.Println("[NO]")
		fmt.Println("[ERROR] Impossibile connettere bot: ", err)
		return
	}
	fmt.Println("[OK]")

	// NOTE Registra callback del bot
	RegisterCallbacks(bot)

	// NOTE Apre sessione discord e comincia ad ascoltare
	fmt.Printf("Connessione a discord ... ")
	err = bot.Open()
	if err != nil {
		fmt.Println("[NO]")
		fmt.Println("[ERROR] Impossibile aprire sessione: ", err)
		return
	}
	fmt.Println("[OK]")

	// NOTE Crea segnale per intercettare la fine del programma
	fmt.Printf("\nBot avviato, premere CTRL + C per terminare\n\n")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)

	<-sc
	fmt.Println("Terminazione Bot")
	bot.Close()
}
