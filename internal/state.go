package internal

import (
	"gitlab.com/filippo.ziche/discordea/internal/data"
)

// NOTE Contiene le anime conosciute e altro
type State struct {
	Anime map[string]data.Anima // Anime conosciute
	Cmds  []data.Comand         // Comandi conosciuti
}

// Istanza globale
var STATE *State

// NOTE Crea nuovo stato globale
func InitializeState() *State {
	STATE = &State{Anime: make(map[string]data.Anima)}
	return STATE
}
