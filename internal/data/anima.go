//	     		    _
//	    /\         (_)
//	   /  \   _ __  _ _ __ ___   __ _
//	  / /\ \ | '_ \| | '_ ` _ \ / _` |
//	 / ____ \| | | | | | | | | | (_| |
//	/_/    \_\_| |_|_|_| |_| |_|\__,_|
//

package data

import (
	"fmt"
	"math"
)

// Definisce un utente conosciuto dell'app
type Anima struct {
	Name  string // nome dell'utente scelto da lui
	Id    string // Id interno a discord
	Level uint32 // Livello del personaggio
	XP    uint32 // XP corrente, necessaria a salire di livello
}

// Aumenta XP utente aumentando il livello
func (a *Anima) IncreaseXP(amount uint32) {

	totalXP := a.XP + amount
	nextLevelXP := NextLevelXP(a.Level)

	// Aggiunge livelli
	for totalXP >= nextLevelXP {
		totalXP -= nextLevelXP
		nextLevelXP = NextLevelXP(a.Level)
		a.Level += 1
	}

	a.XP = totalXP
}

// Ottiene una stringa che descrive l'anima
func (a *Anima) Stringify() string {
	return fmt.Sprintf("Name: %s, Id: %s, Level: %d, XP: %d", a.Name, a.Id, a.Level, a.XP)
}

// Ottiene XP necessari per salire di livello dal livello specificato
func NextLevelXP(level uint32) uint32 {
	a := math.Round(float64(4*math.Pow(float64(level), 3)) / 5)
	return uint32(a)
}
