package data

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

// NOTE Funzione che implementa un comando
type CmdFunc func(s *discordgo.Session, m *discordgo.MessageCreate)

// NOTE Rappresenta un comando, con gli attivatori e la funzione
type Comand struct {
	Activators []string // Comandi che lo attivato
	Action     CmdFunc  // Funzione da eseguire all'attivazione
}

// NOTE ritorna true se il testo attiva il comando
func (cmd *Comand) Activate(content string) bool {
	for _, activator := range cmd.Activators {
		if strings.HasPrefix(activator, content) {
			return true
		}
	}
	return false
}

// NOTE Crea un nuovo comando
func CmdBuild(activators []string, action CmdFunc) Comand {
	return Comand{Activators: activators, Action: action}
}
