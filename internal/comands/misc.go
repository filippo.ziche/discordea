package comands

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/filippo.ziche/discordea/internal"
	"gitlab.com/filippo.ziche/discordea/internal/data"
)

// NOTE Invia un messaggio contenente la stringa "pong"
func Ping(s *discordgo.Session, m *discordgo.MessageCreate) {
	s.ChannelMessageSend(m.ChannelID, "pong!")
}

// NOTE Invia un messaggio contenente il canale attuale
func Info(s *discordgo.Session, m *discordgo.MessageCreate) {

	// In teoria sappiamo che non è nullo èerché inviato da
	// un utente
	if m.Author != nil {

		// Ottiene canale dove è stato inviato
		channel, err := s.Channel(m.ChannelID)
		if err != nil {
			fmt.Println("[ERROR] Canale non ottenibile: ", err)
			return
		}

		// Ottiene guild dove è stato inviato
		guild, err := s.Guild(m.GuildID)
		if err != nil {
			fmt.Println("[ERROR] Canale non ottenibile: ", err)
			return
		}

		response := fmt.Sprintf("User: %s, Channel: %s, Guild: %s", m.Author.Mention(), channel.Mention(), guild.Name)
		s.ChannelMessageSend(m.ChannelID, response)
	}
}

// INFO Ottiene informazioni sull'autore del messaggio
func Anima(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ottiene stato dell'app
	state := internal.STATE

	// Ottiene l'anima di questo utente
	anima, found := state.Anime[m.Author.ID]
	if !found {
		// NOTE Crea una nuova anima associata
		anima = data.Anima{Name: m.Author.Username, Id: m.Author.ID, Level: 0, XP: 0}
		state.Anime[m.Author.ID] = anima

		fmt.Println("[INFO] Creata nuova anima per ", anima.Name)
	}

	s.ChannelMessageSend(m.ChannelID, anima.Stringify())
}
